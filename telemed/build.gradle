/*
 * Copyright (C) 2018 Henrik Bærbak Christensen, baerbak.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

/*
 * Build file for Broker Demo applications, by Henrik Baerbak Christensen.
 */

apply plugin: 'java'

repositories {
    jcenter()
}

dependencies {
  compile project(':broker')
  // Bind SLF4J it to the Log4J logging framework
  compile group: 'org.slf4j', name: 'slf4j-log4j12', version: '1.7.25'

  testCompile 'junit:junit:4.13.1'
  testCompile 'org.hamcrest:hamcrest-library:1.3'
}


// === Demo of Socket based TeleMed system

task serverSocket(type: JavaExec) {
  group 'demo'
  description 'Run socket based TeleMed server'

  classpath sourceSets.test.runtimeClasspath
  main = 'telemed.main.ServerMainSocket'
  args db
}

task homeSocket(type: JavaExec) {
  group 'demo'
  description 'Run a single TeleMed client operation (Socket)'

  classpath sourceSets.test.runtimeClasspath
  main = 'telemed.main.HomeClientSocket'
  args op, id, sys, dia, host
}

// === Demo of HTTP/URI Tunnel based TeleMed system

task serverHttp(type: JavaExec) {
  group 'demo'
  description 'Run Http/URI Tunnel based TeleMed server'

  classpath sourceSets.test.runtimeClasspath
  main = 'telemed.main.ServerMainHTTP'
  args db
}

task homeHttp(type: JavaExec) {
  group 'demo'
  description 'Run a single TeleMed client operation (Http)'

  classpath sourceSets.test.runtimeClasspath
  main = 'telemed.main.HomeClientHTTP'
  args op, id, sys, dia, host
}
